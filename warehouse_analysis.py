#!/bin/python

import pandas as pd
import json
from pprint import pprint


class OrderAnalysis:
    def __init__(self, file):
        self.file = file
        with open(self.file, 'r') as json_data:
            self.json = json.load(json_data)
        self.df = pd.DataFrame(self.json)

    def show_warehouses_rates(self, to_dict=False):
        '''
        task 1:
        Найти тариф стоимости доставки для каждого склада
        '''
        df = self.df
        warehouses_rates = {}
        for index, row in df.iterrows():
            if row['warehouse_name'] not in warehouses_rates:
                products = row['products']
                highway_cost = row['highway_cost']
                total_quantity = sum([product['quantity'] for product in products])

                warehouses_rates[row['warehouse_name']] = highway_cost / total_quantity if total_quantity != 0 else 1
        rates = pd.DataFrame(data=list(warehouses_rates.items()), columns=['name', 'rate'])
        return rates if not to_dict else warehouses_rates

    def show_warehouses_statistics(self):
        '''
        task 2:
        Найти суммарное количество , суммарный доход , суммарный расход 
        и суммарную прибыль для каждого товара 
        (представить как таблицу со столбцами
        'product', 'quantity', 'income', 'expenses', 'profit')
        income = product['price'] * product['quantity']
        expenses = highway_cost * product['quantity']
        profit = income - expenses
        '''
        df = self.df
        rates = self.show_warehouses_rates(to_dict=True)
        statistics = []
        for index, row in df.iterrows():
            highway_cost = row['highway_cost']
            warehouse_name = row['warehouse_name']
            rate = rates[warehouse_name]

            for data in row['products']:
                product = data['product']
                quantity = data['quantity']
                income = data['price'] * quantity
                expenses = abs(rate) * quantity  # same value with highway_cost field
                profit = income - expenses

                statistics.append(
                    {
                        'product': product,
                        'quantity': quantity,
                        'income': income,
                        'expenses': expenses,
                        'profit': profit,
                    }
                )
        data = pd.DataFrame(statistics)
        total_statistics = data.groupby('product').sum().reset_index()
        return total_statistics

    def show_avg_profit_table(self):
        '''
        task 3:
        Составить табличку со столбцами 'order_id' (id заказа) 
        и 'order_profit' (прибыль полученная с заказа). 
        А также вывести среднюю прибыль заказов
        '''
        df = self.df
        avg_profit = []
        rates = self.show_warehouses_rates(to_dict=True)

        for index, row in df.iterrows():
            order_id = row['order_id']
            rate = rates[row['warehouse_name']]
            for data in row['products']:
                profit = (data['price'] * data['quantity']) - (abs(rate) * data['quantity'])

                avg_profit.append(
                    {
                        'order_id': order_id,
                        'profit': profit
                    }
                )
        profit_table = pd.DataFrame(avg_profit).groupby('order_id').sum().reset_index()
        # adding last row with the average profit
        avg_value = profit_table['profit'].mean()
        avg_value_row = pd.DataFrame({'order_id': 'Average profit', 'profit': [avg_value]})
        profit_table = pd.concat([profit_table, avg_value_row]).reset_index(drop=True)
        return profit_table

    def show_warehouses_percent_product(self):
        '''
        task 4:
        Составить табличку типа:
        'warehouse_name', 'product', 'quantity', 'profit', 'percent_profit_product_of_warehouse'
        (процент прибыли продукта заказанного из определенного склада к прибыли этого склада)
        '''
        def calculate_percent(product_profit, total_warehouse_profit):
            percent = (product_profit / total_warehouse_profit) * 100
            return percent

        df = self.df
        rates = self.show_warehouses_rates(to_dict=True)
        warehouses_profit = []

        for index, row in df.iterrows():
            warehouse_name = row['warehouse_name']
            rate = rates[warehouse_name]
            for data in row['products']:
                product = data['product']
                quantity = data['quantity']
                profit = (data['price'] * quantity) - (abs(rate) * quantity)


                warehouses_profit.append(
                    {
                        'warehouse_name': warehouse_name,
                        'product': product,
                        'quantity': quantity,
                        'profit': profit,
                    }
                )

        table = pd.DataFrame(warehouses_profit).groupby(['warehouse_name', 'product'], as_index=False).sum()
        # get total profit values from each warehouse
        warehouses_total_profit = table.groupby('warehouse_name')['profit'].transform('sum')
        new_col = 'percent_profit_product_of_warehouse'
        table[new_col] = calculate_percent(product_profit=table['profit'], total_warehouse_profit=warehouses_total_profit)
        return table


    def show_sorted_warehouse_profit_table(self, table):
        """
        task 5:
        Взять предыдущую табличку и отсортировать 'percent_profit_product_of_warehouse' 
        по убыванию, после посчитать накопленный процент. 
        Накопленный процент - это новый столбец в этой табличке, 
        который должен называться 'accumulated_percent_profit_product_of_warehouse'. 
        По своей сути это постоянно растущая сумма отсортированного 
        по убыванию столбца 'percent_profit_product_of_warehouse'.
        """
        table = table.sort_values(by=['warehouse_name', 'percent_profit_product_of_warehouse'], ascending=[True, False])
        table['accumulated_percent_profit_product_of_warehouse'] = table.groupby('warehouse_name')['percent_profit_product_of_warehouse'].cumsum()
        return table

    def show_categories(self, table):
        """
        task 6:
        Присвоить A,B,C - категории на основании значения накопленного процента 
        ('accumulated_percent_profit_product_of_warehouse'). 
        Если значение накопленного процента меньше или равно 70, то категория A.
        Если от 70 до 90 (включая 90), то категория Б. 
        Остальное - категория C. 
        Новый столбец обозначить в таблице как 'category'
        """
        categories = []
        for index, row in table.iterrows():
            accumulated = row['accumulated_percent_profit_product_of_warehouse']
            if accumulated <= 70:
                categories.append('A')
            elif accumulated > 70 and accumulated <= 90:
                categories.append('B')
            else:
                categories.append('C')
        table['categories'] = categories
        return table


if __name__ == '__main__':
    def result_print(task):
        print(f'\n{task}\n')


    filename = 'trial_task.json'
    o = OrderAnalysis(file=filename)
        # task 1
    task_1 = o.show_warehouses_rates()
    print('TASK_1')
    result_print(task=task_1)
        # task 2
    task_2 = o.show_warehouses_statistics()
    print('TASK_2')
    result_print(task=task_2)
        # task 3
    task_3 = o.show_avg_profit_table()
    print('TASK_3')
    result_print(task=task_3)
        # task 4
    task_4 = o.show_warehouses_percent_product()
    print('TASK_4')
    result_print(task=task_4)
        # task 5
    task_5 = o.show_sorted_warehouse_profit_table(table=task_4)
    print('TASK_5')
    result_print(task=task_5)
        # task_6
    task_6 = o.show_categories(table=task_5)
    print('TASK_6')
    result_print(task=task_6)